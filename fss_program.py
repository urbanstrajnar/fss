# requires netcdf4-python (netcdf4-python.googlecode.com)
from netCDF4 import Dataset as NetCDFFile
from math import log10
# ta spremenljivka pove, koliko datotek pregledamo
# najprej odpre CMORPH_0, odpre pa se vse do STEVILO_DATOTEK - 1
OD_DATOTEKE = 1
DO_DATOTEKE = 2

#===============================================================================
# Navodila: naredimo objekt FSSKalkulator, na primer a = FSSKalkulator(),
# ki mu lahko povemo se argument pot, ki je pot do folderja, ki vsebuje datoteke
# CMORPH_X in ERAInt_X.
# Potem na objektu a poklicemo metodo a.main()
#===============================================================================
class FSSKalkulator():
    """Objekt, ki racuna FSS za dolocene datoteke."""
    
    def __init__(self, pot = "C:/Users/Kdorkoli/Documents/CCda/faks/seminar/"):
        """ Metodi lahko povemo pot do datotek, tako da ji povemo argument 
        'pot' do datotek v obliki stringa, ko naredimo objekt. """
        # poti do datotek
        self.file_mod=[pot + "CMORPH_" + str(i) + ".nc" for i in range(11)]
        self.file_opa=[pot + "ERAInt_" + str(i) + ".nc" for i in range(11)]
        # thresholdi v mm
        #self.thres_list = [0.1, 0.5, 1, 2, 5]
        self.thres_list = [1]

        
    def odpri(self, indeks, thres):
        """ Metoda odpre 2 datoteki s trenutnim indeksom ter njuna binarna polja shrani v objekt self,
        dokler jih z naslednjim klicem metode odpri ne prepise."""
        
        #samo enkrat odpremo datoteko, sicer privzamemo, da ze poznamo podatke o polju padavin
        if thres == self.thres_list[0]:
            nc_mod = NetCDFFile(self.file_mod[indeks])
            nc_opa = NetCDFFile(self.file_opa[indeks])
            prcpvar_mod = nc_mod.variables['precip']
            prcpvar_opa = nc_opa.variables['precip']
            
            # v spremenljivkah data so zapisane padavine
            self.data_mod = prcpvar_mod[:]
            self.data_opa = prcpvar_opa[:]
            
            # N je stevilo stolpcev, M je stevilo vrstic
            (self.N, self.M) = (len(self.data_mod[0]), len(self.data_mod))
            
            print("N = {}, M = {}".format(self.N, self.M))

            # to sta binarna polja, ki sta odvisna od velikosti thresholda
            self.binary_mod = [[1 if self.data_mod[j][i] >= thres else 0 for i in range(self.N)]for j in range(self.M)]
            self.binary_opa = [[1 if self.data_opa[j][i] >= thres else 0 for i in range(self.N)]for j in range(self.M)]
            # naredimo se prazna polja za racunanje delezev z metodo racunaj_deleze
            self.frac_mod = [[0 for i in range(self.N)] for j in range(self.M)]
            self.frac_opa = [[0 for i in range(self.N)] for j in range(self.M)]
            # prazni polji za prejsnje vsote
            self.vsota_mod_prev = [[0 for i in range(self.N)] for j in range(self.M)]
            self.vsota_opa_prev = [[0 for i in range(self.N)] for j in range(self.M)]
            # zapri datoteki
            nc_mod.close()
            nc_opa.close()
            # poisci daljso stranico
            self.max = 2* max(self.N, self.M) - 1
            # seznam, n-jev, ki jih bomo obravnavali
            self.n_list = self.seznam_n()
        else:
            for j in range(self.M):
                for i in range(self.N):
                        if self.data_mod[j][i] < thres:
                            self.binary_mod[j][i] = 0
                        if self.data_opa[j][i] < thres:
                            self.binary_opa[j][i] = 0
    
    
    def seznam_n(self):
        seznam = [1]
        meja = 0
        for i in range(2, self.max):
            if i % 2 != 0:
                seznam.append(i)
                #===============================================================
                # if i < 20:
                #     if i - 2**meja > seznam[len(seznam) - 1]:
                #         seznam.append(i)
                #         meja = log10(i)
                #===============================================================
                    
        
        print(seznam)
        return seznam                        

    def racunaj_deleze(self, n, index):
        """ Metoda racuna polja delezev za velikost kvadrata n
        in klice drugo metodo, ki hkrati racuna FSS"""
        if index > 0:
            prejsnji_n = self.n_list[index - 1]
            stranica_polovic = (prejsnji_n - 1) // 2 
        for j in range(self.M):
            for i in range(self.N):
                vsota_mod = 0
                vsota_opa = 0
                k = 1
                while k <= n:
                    for l in range(1,n+1):
                        x = i + k - 1 - int((n-1)/2)
                        y = j + l - 1 - int((n-1)/2)
                        #=======================================================
                        # if index > 0:
                        #=======================================================
                            #===================================================
                            # if n > 1 and i < 1 and j < 1:
                            #         print("n = {},  i = {}, j = {} y = {} x = {}".format(prejsnji_n, i, j, y, x))
                            #===================================================
                            #Preveri, ce smo to polje ze izracunali in se premakni naprej
                            #===================================================
                            # if x == i - stranica_polovic and (y >= j - stranica_polovic and y <= j + stranica_polovic):
                            #     x = i + k - 1 - int((n-1)/2) + 2 * stranica_polovic + 1
                            #===================================================
                                #===============================================
                                # if n > 1 and i < 1 and j < 1:
                                #     print("n = {}, i = {}, j = {}, y = {} nov x = {}".format(prejsnji_n, i, j, y, x))
                                #===============================================
                        if x >= 0 and x < self.N and y >= 0 and y < self.M:
                            vsota_mod += self.binary_mod[y][x]
                            vsota_opa += self.binary_opa[y][x]
                    k+=1
                #===============================================================
                # if index > 0:
                #     #dodaj ze preverjeno polje trenutni vsoti
                #     vsota_mod += self.vsota_mod_prev[j][i]
                #     vsota_opa += self.vsota_opa_prev[j][i]
                #===============================================================
                #===============================================================
                # self.vsota_mod_prev[j][i] = vsota_mod
                # self.vsota_opa_prev[j][i] = vsota_opa
                #===============================================================
                self.frac_mod[j][i] = vsota_mod / (n*n)
                self.frac_opa[j][i] = vsota_opa / (n*n)
                if n > 50:
                    print("i = {}, j = {}".format(i,j))
            

    def racunaj_fss(self):
        """ Metoda izračuna FSS po enačbi """
        vsota = 0
        sum = 0
        for j in range(self.M):
                for i in range(self.N):
                    vsota += self.frac_mod[j][i] ** 2 + self.frac_opa[j][i] ** 2
                    sum += (self.frac_mod[j][i] - self.frac_opa[j][i]) ** 2
        return 1 - (sum / (self.N * self.M)) / ((vsota) / (self.N * self.M))
    
    def main(self):
        """ Metoda poklice vse potrebne metode za izracun FSS in sproti zapisuje rezultate v datoteko."""
        with open(r"C:\Users\Kdorkoli\Documents\CCda\faks\seminar\fss.txt", "w") as f:
            f.write("Stev\tthreshold\tn\tFSS\n")
            for indeks in range(OD_DATOTEKE,DO_DATOTEKE):       
                for thres in self.thres_list:
                    koncaj = False
                    prejsnji_fss = 1000
                    self.odpri(indeks, thres)
                    # v tej for zanki lahko spremenimo za katere stranice kvadrata
                    # program racuna FSS.
                    # ce je range(1,2 * vecji - 1, 2) bo pregledal vse mozne lihe velikosti do
                    # tistega, ki vsebuje vse elemente in bo FSS najvecji mozen, odvisno
                    # od ujemanje frekvence prekoracitve thresholdov v obeh binarnih poljih
                    for i in range(len(self.n_list)):
                        if not koncaj:
                            print("n = {0}".format(self.n_list[i]))
                            self.racunaj_deleze(self.n_list[i], i)
                            fss = self.racunaj_fss()
                            f.write("{0}\t{1}\t{2}\t{3}\n".format(indeks, thres, self.n_list[i], fss))
                            print(abs(prejsnji_fss - fss))
                            if abs(prejsnji_fss - fss) < 0.005:
                                koncaj = True
                            prejsnji_fss = fss
                    f.write("\n")

a = FSSKalkulator()
a.main()
