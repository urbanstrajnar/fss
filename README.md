# README #


### Repozitorij FSS ###
To je repozitorij za seminarsko nalogo **Verifikacija padavisnkih modelov z metodo Fraction Skill Score**.

Avtor: Urban Strajnar  
Mentor: Gregor Skok

##Python##
[program_fss.py](https://bitbucket.org/urbanstrajnar/fss/src/20f129f115fe1d7c831eecba854ede810c5ecfb0/fss_program.py?at=master) je program za računanje FSS.

##LaTex##

Datoteki [seminar_fss.tex](https://bitbucket.org/urbanstrajnar/fss/src/20f129f115fe1d7c831eecba854ede810c5ecfb0/seminar_fss.tex?at=master) in [title_seminar.tex](https://bitbucket.org/urbanstrajnar/fss/src/20f129f115fe1d7c831eecba854ede810c5ecfb0/title_seminar.tex?at=master) sta izvorni kodi za seminarsko nalogo.